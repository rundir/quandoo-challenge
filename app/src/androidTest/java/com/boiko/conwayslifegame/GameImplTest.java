package com.boiko.conwayslifegame;


import android.os.Handler;

import org.hamcrest.MatcherAssert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Unittest for {@link GameImpl}.
 */
@RunWith(MockitoJUnitRunner.class)
public class GameImplTest {
  private final NonThreadExecutor nonThreadExecutor;
  @Mock
  private Algorithm mockAlgorithm;
  @Mock
  private Game.UpdateListener mockUpdateListener;
  @Mock
  private ExecutorService mockExecutorService;

  public GameImplTest() {
    nonThreadExecutor = new NonThreadExecutor();
  }

  /**
   * GIVEN A {@link GameImpl} instance
   * WHEN I call {@link GameImpl#onStart()} method
   * THEN I expect that a new task will be submitted.
   */
  @Test
  public void testSubmitMethodCalled() throws Exception {
    final Game game = new GameImpl(mockAlgorithm, mockUpdateListener, mockExecutorService, Mockito.mock(Handler.class));
    game.onStart();

    Mockito.verify(mockExecutorService, Mockito.times(1)).submit(Mockito.any(Runnable.class));
  }

  /**
   * GIVEN A {@link GameImpl} instance
   * WHEN I call {@link GameImpl#onDestroy()} method
   * THEN I expect that an executor will be shutdowm.
   */
  @Test
  public void testShutdownMethodCalled() throws Exception {
    final Game game = new GameImpl(mockAlgorithm, mockUpdateListener, mockExecutorService, Mockito.mock(Handler.class));
    game.onDestroy();

    Mockito.verify(mockExecutorService, Mockito.times(1)).shutdown();
  }

  /**
   * GIVEN A {@link GameImpl} instance
   * WHEN I call {@link GameImpl#toggle()} method
   * THEN I expect that it will change the game state.
   */
  @Test
  public void testToggleMethodChangesState() throws Exception {
    final Game game = new GameImpl(mockAlgorithm, mockUpdateListener, mockExecutorService, Mockito.mock(Handler.class));
    final boolean initialState = game.isSimulationRunning();
    game.toggle();

    MatcherAssert.assertThat("The game state wasn't changed", initialState != game.isSimulationRunning());
  }

  /**
   * GIVEN A {@link GameImpl} instance
   * WHEN I call {@link GameImpl#getDataStorage()} method
   * THEN I expect that it will call the {@link Algorithm} getter.
   */
  @Test
  public void testAlgorithmGetDataCalled() throws Exception {
    final Game game = new GameImpl(mockAlgorithm, mockUpdateListener, mockExecutorService, Mockito.mock(Handler.class));
    game.getDataStorage();

    Mockito.verify(mockAlgorithm, Mockito.times(1)).getDataStorage();
  }

  /**
   * GIVEN A {@link GameImpl} instance
   * WHEN user touches the cell
   * THEN I expect  the {@link Algorithm#touchCell(int, int)} and {@link Algorithm#getDataStorage()} will be called.
   */
  @Test
  public void testOnTouchMethod() throws Exception {
    final Game game = new GameImpl(mockAlgorithm, mockUpdateListener, mockExecutorService, Mockito.mock(Handler.class));
    final int someX = 0;
    final int someY = 0;
    game.onCellTouch(someX, someY);

    Mockito.verify(mockAlgorithm, Mockito.times(1)).touchCell(someX, someY);
    Mockito.verify(mockAlgorithm, Mockito.times(1)).getDataStorage();
  }

  /**
   * {@link java.util.concurrent.Executor} that executes {@link Runnable}s immediately on the current thread.
   */
  private static class NonThreadExecutor extends ThreadPoolExecutor {
    public NonThreadExecutor() {
      super(1, 1, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
    }

    @Override
    public void execute(final Runnable command) {
      command.run();
    }
  }

}