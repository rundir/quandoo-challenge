package com.boiko.conwayslifegame;

import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Instrumentational tests for {@link MainActivity}.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityEspressoTest {
  @Rule
  public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

  /**
   * GIVEN main application
   * WHEN user starts it
   * THEN the user expects to see the game field and the button to toggle.
   */
  @Test
  public void testDefaultView() {
    onView(withId(R.id.field)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
    onView(withId(R.id.btnStart)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
  }

  /**
   * GIVEN main application
   * WHEN user starts presses the button to start the game
   * THEN I expect that the button will change the title.
   */
  @Test
  public void testMainButton() {
    onView(withId(R.id.btnStart)).check((ViewAssertions.matches(ViewMatchers.withText(R.string.btn_name_start))));
    onView(withId(R.id.btnStart)).perform(click());
    onView(withId(R.id.btnStart)).check((ViewAssertions.matches(ViewMatchers.withText(R.string.btn_name_stop))));
  }
}
