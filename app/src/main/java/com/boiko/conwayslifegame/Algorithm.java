package com.boiko.conwayslifegame;

/**
 * Interface that controls data.
 */
public interface Algorithm {
  /**
   * Calculates the next generation of the model data.
   */
  void nextGeneration();

  /**
   * The model getter
   *
   * @return the current model.
   */
  DataStorage getDataStorage();

  /**
   * Changes the current state of a cell pointed by the given coordinates
   *
   * @param i a cell i-coordinate
   * @param j a cell j-coordinate.
   */
  void touchCell(int i, int j);
}
