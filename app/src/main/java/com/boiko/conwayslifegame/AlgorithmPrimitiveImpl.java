package com.boiko.conwayslifegame;

/**
 * Basic implementation of {@link Algorithm} that uses {@link DataStorage} as a storage.
 */
public class AlgorithmPrimitiveImpl implements Algorithm {
  private DataStorage dataStorage;

  public AlgorithmPrimitiveImpl(final DataStorage dataStorage) {
    this.dataStorage = dataStorage;
  }

  @Override
  public void nextGeneration() {
    final boolean[][] oldCells = dataStorage.getTwoDimensionArray();
    final boolean[][] newCells = new boolean[oldCells.length][];
    for (int i = 0; i < oldCells.length; i++) {
      newCells[i] = new boolean[oldCells.length];
      for (int j = 0; j < oldCells[i].length; j++) {
        final int neighbours = neighboursCount(oldCells, i, j);
        if (oldCells[i][j]) {
          newCells[i][j] = ((neighbours == 2) || (neighbours == 3));
        } else {
          newCells[i][j] = (neighbours == 3);
        }
      }
    }
    dataStorage = new TwoDimensionArray(newCells);
  }

  @Override
  public DataStorage getDataStorage() {
    return dataStorage;
  }

  @Override
  public void touchCell(final int i, final int j) {
    dataStorage.touchCell(i, j);
  }

  private int neighboursCount(final boolean[][] cells, final int x, final int y) {
    int neighboursCount = 0;

    for (int i = x - 1; i <= x + 1; i++) {
      final int ii;
      if (i < 0) {
        ii = cells.length - 1;
      } else if (i >= cells.length) {
        ii = 0;
      } else {
        ii = i;
      }
      for (int j = y - 1; j <= y + 1; j++) {
        final int jj;
        if (j < 0) {
          jj = cells[ii].length - 1;
        } else if (j >= cells[ii].length) {
          jj = 0;
        } else {
          jj = j;
        }
        if ((ii == x) && (jj == y)) continue;
        if (cells[ii][jj]) neighboursCount++;
      }
    }

    return neighboursCount;
  }
}