package com.boiko.conwayslifegame;

import android.os.Parcelable;

/**
 * Data storage that represent a data like a two-dimension array.
 */
public interface DataStorage extends Parcelable {
  /**
   * @return a data like two-dimension array of booleans.
   */
  boolean[][] getTwoDimensionArray();

  /**
   * Changes the current state of a cell pointed by the given coordinates.
   *
   * @param i a cell i-coordinate.
   * @param j a cell j-coordinate.
   */
  void touchCell(int i, int j);
}
