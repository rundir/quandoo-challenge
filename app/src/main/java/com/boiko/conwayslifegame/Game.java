package com.boiko.conwayslifegame;

/**
 * Conway's life game controller.
 */
public interface Game {
  /**
   * @return {@code true} if simulation is running.
   */
  boolean isSimulationRunning();

  /**
   * Changes the current state of the game.
   */
  void toggle();

  /**
   * Called when the game is visible to the user.
   */
  void onStart();

  /**
   * Called when the game is no longer started.
   */
  void onStop();

  /**
   * Called when the game is no longer in use.
   */
  void onDestroy();

  /**
   * @return the current model data.
   */
  DataStorage getDataStorage();

  /**
   * Called when the user touches the game board.
   *
   * @param i a cell i-coordinate.
   * @param j a cell j-coordinate.
   */
  void onCellTouch(int i, int j);

  /**
   * The data model update listener.
   */
  interface UpdateListener {
    /**
     * Called when the data set has been changed.
     *
     * @param dataStorage the data model.
     */
    void onUpdate(DataStorage dataStorage);
  }
}
