package com.boiko.conwayslifegame;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

/**
 * A view that displays the current game state.
 */
public class GameFieldView extends SurfaceView implements Runnable, SurfaceHolder.Callback, View.OnTouchListener {
  private Thread thread;
  private SurfaceHolder surfaceHolder;
  private volatile boolean running;
  private OnCellTouchListener onCellTouchListener;

  private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

  private DataStorage dataStorage;
  private volatile boolean dirty = true;

  public GameFieldView(Context context) {
    super(context);
    initView();
  }

  public GameFieldView(final Context context, final AttributeSet attrs) {
    super(context, attrs);
    initView();
  }

  public GameFieldView(final Context context, final AttributeSet attrs, final int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    initView();
  }

  private void initView() {
    surfaceHolder = getHolder();
    getHolder().addCallback(this);
    setOnTouchListener(this);
  }

  public void setDataStorage(final DataStorage dataStorage) {
    this.dataStorage = dataStorage;
    invalidateField();
  }

  private void invalidateField() {
    dirty = true;
  }

  /**
   * Register a callback to be invoked when a touch event is sent to this board.
   *
   * @param onCellTouchListener a listener that will be called in case of cell touch.
   */
  public void setOnCellTouchListener(final OnCellTouchListener onCellTouchListener) {
    this.onCellTouchListener = onCellTouchListener;
  }

  @Override
  public void run() {
    while (running) {
      if (dirty) {
        onDrawCells();
        dirty = false;
      }
    }
  }

  @Override
  protected void onLayout(final boolean changed, final int left, final int top, final int right, final int bottom) {
    super.onLayout(changed, left, top, right, bottom);
    invalidateField();
  }

  private void onDrawCells() {
    if (surfaceHolder.getSurface().isValid()) {
      Canvas canvas = null;
      try {
        canvas = surfaceHolder.lockCanvas();

        if (canvas == null) return;

        canvas.drawColor(Color.GRAY);
        if (dataStorage != null) {
          Rect rect = surfaceHolder.getSurfaceFrame();
          final int fieldWidth = (rect.height() > rect.width()) ? rect.width() : rect.height();
          final int vertPadding = (rect.height() - fieldWidth) / 2;
          final int horzPadding = (rect.width() - fieldWidth) / 2;

          final boolean[][] cells = dataStorage.getTwoDimensionArray();
          for (int i = 0; i < cells.length; i++) {
            final int cellWidth = fieldWidth / cells.length;
            for (int j = 0; j < cells[i].length; j++) {
              final int left = i * cellWidth + horzPadding;
              final int top = j * cellWidth + vertPadding;
              final int right = left + cellWidth;
              final int bottom = top + cellWidth;

              if (!running) return;

              paint.setStyle(Paint.Style.FILL);
              paint.setColor(cells[i][j] ? Color.GREEN : Color.WHITE);
              canvas.drawRect(left, top, right, bottom, paint);

              paint.setStyle(Paint.Style.STROKE);
              paint.setColor(Color.BLACK);
              canvas.drawRect(left, top, right, bottom, paint);
            }
          }
        }
      } finally {
        if (canvas != null) surfaceHolder.unlockCanvasAndPost(canvas);
      }
    }
  }

  @Override
  public void surfaceCreated(final SurfaceHolder holder) {
    invalidateField();
    running = true;
    thread = new Thread(this);
    thread.start();
  }

  @Override
  public void surfaceChanged(final SurfaceHolder holder, final int format, final int width, final int height) {
  }

  @Override
  public void surfaceDestroyed(final SurfaceHolder holder) {
    running = false;

    boolean retry = true;
    while (retry) {
      try {
        thread.join();
        retry = false;
      } catch (final InterruptedException ignored) {
      }
    }
  }

  @Override
  public boolean onTouch(final View v, final MotionEvent event) {
    if (event.getAction() == MotionEvent.ACTION_DOWN) {
      final int x = (int) event.getX();
      final int y = (int) event.getY();

      Rect rect = surfaceHolder.getSurfaceFrame();
      final int fieldWidth = (rect.height() > rect.width()) ? rect.width() : rect.height();

      final boolean[][] cells = dataStorage.getTwoDimensionArray();
      final int vertPadding = (rect.height() - fieldWidth) / 2;
      final int horzPadding = (rect.width() - fieldWidth) / 2;

      if ((x < horzPadding) || (x > (horzPadding + fieldWidth)) || (y < vertPadding) || (y > (vertPadding + fieldWidth)))
        return false;

      final int i = (x - horzPadding) * cells.length / fieldWidth;
      final int j = (y - vertPadding) * cells.length / fieldWidth;

      onCellTouchListener.onCellTouch(dataStorage, i, j);
      return true;
    }

    return false;
  }

  /**
   * Interface definition for a callback to be invoked when a touch event is dispatched on a cell.
   */
  public interface OnCellTouchListener {
    /**
     * Called when a touch even is dispatched on a cell
     *
     * @param dataStorage a data model that is uses.
     * @param i           a cell i-coordinate.
     * @param j           a cell j-coordinate.
     */
    void onCellTouch(DataStorage dataStorage, int i, int j);
  }
}
