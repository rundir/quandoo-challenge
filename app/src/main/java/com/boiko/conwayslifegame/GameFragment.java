package com.boiko.conwayslifegame;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Fragment that contains game field and manages {@link Game} lifecycle.
 */
public class GameFragment extends Fragment implements Game.UpdateListener {
  public static final String DATA_STORAGE = "DATA_STORAGE";
  public static final String GAME_RUNNING = "GAME_RUNNING";

  private Game game;
  private GameFieldView fieldView;

  public GameFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @return A new instance of fragment GameFragment.
   */
  public static GameFragment newInstance() {
    GameFragment fragment = new GameFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    final TwoDimensionArray dataStorage;
    boolean running = false;
    if (savedInstanceState != null) {
      dataStorage = savedInstanceState.getParcelable(DATA_STORAGE);
      running = savedInstanceState.getBoolean(GAME_RUNNING, false);
    } else {
      dataStorage = new TwoDimensionArray();
    }
    game = new GameImpl(new AlgorithmPrimitiveImpl(dataStorage), this);
    if (running) game.toggle();
  }

  @Override
  public void onStart() {
    super.onStart();
    game.onStart();
  }

  @Override
  public void onStop() {
    super.onStop();
    game.onStop();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    game.onDestroy();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_game, container, false);
  }

  @Override
  public void onViewCreated(final View view, final Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    fieldView = (GameFieldView) view.findViewById(R.id.field);
    fieldView.setDataStorage(game.getDataStorage());
    fieldView.setOnCellTouchListener(new GameFieldView.OnCellTouchListener() {
      @Override
      public void onCellTouch(final DataStorage dataStorage, final int i, final int j) {
        game.onCellTouch(i, j);
      }
    });
    final Button btnStart = (Button) view.findViewById(R.id.btnStart);
    btnStart.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View v) {
        game.toggle();
        btnStart.setText(game.isSimulationRunning() ? getResources().getString(R.string.btn_name_stop)
            : getResources().getString(R.string.btn_name_start));
      }
    });
    btnStart.setText(game.isSimulationRunning() ? getResources().getString(R.string.btn_name_stop)
        : getResources().getString(R.string.btn_name_start));
  }

  @Override
  public void onSaveInstanceState(final Bundle outState) {
    outState.putParcelable(DATA_STORAGE, game.getDataStorage());
    outState.putBoolean(GAME_RUNNING, game.isSimulationRunning());
    super.onSaveInstanceState(outState);
  }

  @Override
  public void onUpdate(final DataStorage dataStorage) {
    fieldView.setDataStorage(dataStorage);
  }
}
