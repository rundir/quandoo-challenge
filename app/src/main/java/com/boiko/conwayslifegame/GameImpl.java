package com.boiko.conwayslifegame;

import android.os.Handler;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Implementation of {@link Game}.
 */
public class GameImpl implements Game, Runnable {
  private final long DEFAULT_TIME_UPDATE = 1000l;
  private final UpdateListener updateListener;
  private final Algorithm algorithm;
  private final Handler handler;
  private volatile boolean isSimulationRunning;
  private boolean isGameStarted;
  private ExecutorService executorService;
  private Future<?> gameFuture;

  public GameImpl(final Algorithm algorithm, final Game.UpdateListener updateListener) {
    this(algorithm, updateListener, Executors.newSingleThreadExecutor(), new Handler());
  }

  public GameImpl(final Algorithm algorithm, final UpdateListener updateListener, final ExecutorService executorService, final Handler handler) {
    this.algorithm = algorithm;
    this.updateListener = updateListener;
    this.executorService = executorService;
    this.handler = handler;
  }

  @Override
  public void run() {
    long lastExecutionTime = 0;
    while (isGameStarted) {
      final long executionTime = System.currentTimeMillis();
      if (isSimulationRunning && executionTime - lastExecutionTime > DEFAULT_TIME_UPDATE) {
        algorithm.nextGeneration();
        updateView(algorithm.getDataStorage());

        lastExecutionTime = executionTime;
      }
    }
  }

  private void updateView(final DataStorage dataStorage) {
    handler.post(new Runnable() {
      @Override
      public void run() {
        if (updateListener != null) {
          updateListener.onUpdate(dataStorage);
        }
      }
    });
  }

  @Override
  public boolean isSimulationRunning() {
    return isSimulationRunning;
  }

  @Override
  public void toggle() {
    isSimulationRunning = !isSimulationRunning;
  }

  @Override
  public void onStart() {
    isGameStarted = true;
    gameFuture = executorService.submit(this);
  }

  @Override
  public void onStop() {
    isGameStarted = false;
    if (gameFuture != null) {
      gameFuture.cancel(true);
    }
  }

  @Override
  public void onDestroy() {
    executorService.shutdown();
  }

  @Override
  public DataStorage getDataStorage() {
    return algorithm.getDataStorage();
  }

  @Override
  public void onCellTouch(final int i, final int j) {
    algorithm.touchCell(i, j);
    updateView(algorithm.getDataStorage());
  }
}
