package com.boiko.conwayslifegame;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

/**
 * The main activity of this application.
 */
public class MainActivity extends AppCompatActivity {
  public static final String GAME_FRAGMENT = "GameFragment";

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    if (savedInstanceState == null) {
      getSupportFragmentManager().beginTransaction().add(R.id.body, GameFragment.newInstance(), GAME_FRAGMENT).commit();
    }
  }
}
