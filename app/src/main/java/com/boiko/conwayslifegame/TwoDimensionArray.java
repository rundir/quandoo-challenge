package com.boiko.conwayslifegame;

import android.os.Parcel;

/**
 * Basic implementation of {@link DataStorage} that uses two-dimension boolean array as a storage.
 */
public class TwoDimensionArray implements DataStorage {
  public static final int MAX_WIDTH = 20;
  public static final int MAX_HEIGHT = 20;
  public static final Creator<TwoDimensionArray> CREATOR = new Creator<TwoDimensionArray>() {
    public TwoDimensionArray createFromParcel(final Parcel source) {
      return new TwoDimensionArray(source);
    }

    public TwoDimensionArray[] newArray(final int size) {
      return new TwoDimensionArray[size];
    }
  };
  private final boolean[][] cells;

  public TwoDimensionArray() {
    cells = new boolean[MAX_WIDTH][MAX_HEIGHT];
    for (int i = 0; i < MAX_WIDTH; i++) {
      for (int j = 0; j < MAX_HEIGHT; j++) {
        cells[i][j] = false;
      }
    }

    cells[0][1] = true;
    cells[1][2] = true;
    cells[2][0] = true;
    cells[2][1] = true;
    cells[2][2] = true;
  }

  public TwoDimensionArray(final boolean[][] cells) {
    this.cells = new boolean[MAX_WIDTH][MAX_HEIGHT];
    for (int i = 0; i < cells.length; i++) {
      System.arraycopy(cells[i], 0, this.cells[i], 0, cells[i].length);
    }
  }

  protected TwoDimensionArray(final Parcel in) {
    final boolean[][] array;
    final int N = in.readInt();
    array = new boolean[N][];
    for (int i = 0; i < N; i++) {
      array[i] = in.createBooleanArray();
    }
    cells = array;
  }

  @Override
  public boolean[][] getTwoDimensionArray() {
    return cells;
  }

  @Override
  public void touchCell(final int i, final int j) {
    cells[i][j] = !cells[i][j];
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(final Parcel dest, final int flags) {
    final int N = cells.length;
    dest.writeInt(N);
    for (final boolean[] cell : cells) {
      dest.writeBooleanArray(cell);
    }
  }
}
